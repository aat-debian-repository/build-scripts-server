#!/bin/bash

# TODO: https://askubuntu.com/a/744988 on tmpfs
# and https://askubuntu.com/a/744988 on tmp (while arch-chroot is active only). No leading /, as you can see
# You can check it in df -h
# 2G of size is fine

#
# AAT-DEBIAN-REPO AUTOMATED BUILD SCRIPT FOR SERVERS (CHROMIUM ONLY)
#
# The MIT License (MIT)
# Copyright © 2022 Antoni Aloy Torrens <aaloytorrens@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”),
# to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
# https://mit-license.org/
#


# We will build packages inside a debian chroot called debian-bullseye-$arch
# NOTES to build the chroot:
# 	* Bootstrap the root filesystem -> `sudo debootstrap --arch=amd64 bullseye debian-bullseye-$arch`
#	* Enter the chroot -> `sudo apt install arch-install-scripts -y && sudo arch-chroot debian-bullseye-$arch /bin/bash`
#	* Inside the chroot:
#		* Install git for downloading needed GitLab repositories -> `sudo apt install git`
#		* Install dpkg-dev and devscripts for building Debian packages -> `sudo apt install devscripts dpkg-dev`
#		* Install dpkg-sig for signing debian and debmake packages -> `sudo apt install dpkg-sig`
#		* Install makedeb -> See https://www.makedeb.org -> Easier way to build package
#		* Install reprepro for setting up Debian repositories -> `sudo apt install reprepro`
#		* Allow sudo group to execute any command without password -> `sudo visudo` and search for it
#		* Create a standard user called builder, no password -> `sudo adduser builder && sudo adduser builder sudo`
#		* Allow sudo commands without password -> `sudo visudo`
#		* Login as builder -> `su - builder`
#		* Clone the repository -> `git clone https://gitlab.com/antoni.aloytorrens/debrepo packages`
#	* Ready to run this script! -> Packages are classified in two groups: the packages that are built the Debian way and the packages built with makedeb.

# Stop at any error
set -eu

# Exit on CTRL+C
trap "exit" INT

# Exit on any error -> FIXME: Some commands, such as adding user, etc. might be taken as errors
#set -eu

# Packaging status (bullseye)
# Chromium -> Enables Hangouts / Google Chat screen sharing extension
# We have made a special script for Chromium, as this package has lots of updates and secfixes.
# Other packages are not that rellevant unless they are bugfix, but this is.

# TODO: Make prebuilt rootfs with all of the above.

# TODO: Check everything instead of removing it!
# We need a flag to remove everything

# Remove everything if we mess up (uncomment this if you want to start again)
#sudo rm -rf debian-bullseye-$arch/home/builder/*

ARCHES=( "amd64" )
REPO_DEPENDENCIES=false
for arch in ${ARCHES[@]}; do
	cd $HOME

	# Report time and date
	echo "STARTED JOB AT $(date +'%Y-%m-%d %H:%M:%S')"

	# Update and cleanup
	echo "Update and cleanup..."
	sudo apt-get update >/dev/null; sudo apt-get -y full-upgrade >/dev/null; sudo apt-get -y autoremove >/dev/null; sudo apt-get clean

	# Remove contents of root (if you do CTRL+C in signing packages, the script behaves abnormally and could write files inside root folder)
	echo "Removing contents of /root in rootfs..."
	sudo rm -rf debian-bullseye-$arch/root/*

	# Copy /etc/hosts file
	echo "Copying /etc/hosts file..."
	sudo cp /etc/hosts debian-bullseye-$arch/etc/hosts

	# Copy sudoers file
	echo "Copying sudoers file..."
	sudo mkdir -p debian-bullseye-$arch/etc/sudoers.d/
	sudo cp etc-conf/sudoers.d/builder debian-bullseye-$arch/etc/sudoers.d/builder

	# Copy sources.list
	echo "Copying Yuzu sources.list (generic sources.list)..."
	sudo cp etc-conf/apt/sources.list debian-bullseye-$arch/etc/apt/sources.list

	# Copy keys
	echo "Copying keys..."
	sudo cp keys/debports.rsa.secret.gpg debian-bullseye-$arch/home/builder/
	sudo cp keys/debports.rsa.pub.gpg debian-bullseye-$arch/home/builder/
	sudo cp usr-conf/share/keyrings/makedeb-archive-keyring.gpg debian-bullseye-$arch/usr/share/keyrings/makedeb-archive-keyring.gpg

	# Check gnupg files
	# https://d.sb/2016/11/gpg-inappropriate-ioctl-for-device-errors
	echo "Checking gnupg..."
	if ! [ -f debian-bullseye-$arch/home/builder/.gnupg/gpg-agent.conf ] || ! [ -f debian-bullseye-$arch/home/builder/.gnupg/gpg.conf ]; then
		sudo touch debian-bullseye-$arch/home/builder/restart-gnupg-service.txt
		sudo mkdir -p debian-bullseye-$arch/home/builder/.gnupg/
		sudo cp gnupg/gpg-agent.conf debian-bullseye-$arch/home/builder/.gnupg/gpg-agent.conf
		sudo cp gnupg/gpg.conf debian-bullseye-$arch/home/builder/.gnupg/gpg.conf
	else
		sudo rm -rf debian-bullseye-$arch/home/builder/restart-gnupg-service.txt
	fi

	# Check repository files
	#if ! [ -d debian-bullseye-$arch/home/builder/repo-conf ]; then
	echo "Checking repository files..."
	sudo cp -r repo-conf debian-bullseye-$arch/home/builder/
	#fi

	# Mount --bind required filesystems (see yuzu debian/rules)
	sudo mkdir -p debian-bullseye-$arch/proc/
	sudo touch debian-bullseye-$arch/proc/cpuinfo
	sudo mount --bind /proc/cpuinfo debian-bullseye-$arch/proc/cpuinfo
	#sudo mount --bind /sys debian-bullseye-$arch/sys

	# Enter the chroot and execute commands
	echo "Done. Entering chroot..."
	sudo arch-chroot debian-bullseye-$arch /bin/bash -x <<'EOF'

	# Stop at any error
	set -eu

	# Try to fix if some attempts were unsuccessful
	echo "Checking for previous failures..."
	dpkg --configure -a >/dev/null
	apt -f install -y >/dev/null
	echo "Done."

	# Add builder user into sudo group
	echo "Adding builder user into sudo group..."
	adduser builder sudo >/dev/null 2>&1

	echo "Updating rootfs, and cleaning up..."
	# Update and cleanup
	apt update >/dev/null; apt-get -y full-upgrade --allow-downgrades >/dev/null; apt-get -y autoremove >/dev/null; apt-get clean

	# NOTE: We will use prebuilt rootfs instead
	#if [ -f /debootstrap/debootstrap ]; then
	#	debootstrap/debootstrap --second-stage
	#fi

	# Install required packages
	echo "Installing required packages..."
	apt-get -yq install locales git dpkg-dev equivs devscripts dpkg-sig reprepro ash >/dev/null

	# Install sudo and ca-certificates for key importing
	echo "Installing sudo and ca-certificates for key importing..."
	apt-get -yq install sudo ca-certificates >/dev/null

	# Configure locales
	echo "Configuring locales..."
	apt-get -yq install debconf-utils >/dev/null 2>&1
	echo "locales locales/default_environment_locale select es_ES.UTF-8" | debconf-set-selections
	echo "locales locales/locales_to_be_generated multiselect es_ES.UTF-8 UTF-8" | debconf-set-selections
	rm "/etc/locale.gen"
	dpkg-reconfigure --frontend noninteractive locales >/dev/null 2>&1

	# Log in as builder
	echo "Logging in as builder..."
	su - builder

	# Change ownership and import keys
	echo "Changing ownership, importing keys..."
	sudo chown -R builder:builder $HOME/debports.rsa.secret.gpg
	sudo chown -R builder:builder $HOME/debports.rsa.pub.gpg

	# Fix gnupg permissions
	echo "Fixing gnupg permissions..."
	sudo chown -R builder:builder $HOME/.gnupg/
	sudo chmod 600 $HOME/.gnupg/*
	sudo chmod 700 $HOME/.gnupg/

	echo RELOADAGENT | gpg-connect-agent

	# --batch: https://superuser.com/questions/1325862/gnupg2-suddenly-throwing-error-building-skey-array-no-such-file-or-directory
	gpg --import --batch debports.rsa.pub.gpg
	gpg --allow-secret-key-import --batch --import debports.rsa.secret.gpg

	# Change owner of repository configurations
	sudo chown -R builder:builder $HOME/repo-conf

	# Retrieve yuzu-build source code
	echo "Retrieving Yuzu-build source code..."
	git clone --depth=1 https://gitlab.com/aat-debian-repository/debian/yuzu-build.git || echo "Already cloned!"

	echo "Building dependencies..."
	cd yuzu-build

	echo "Copying bp-script.sh..."
	sudo cp bp-script.sh /usr/bin/bp
	sudo chmod +x /usr/bin/bp

	echo "Installing libpipewire-0.3-dev from backports (workaround for SDL2)"
	sudo apt-get -t bullseye-backports -y install libpipewire-0.3-dev

	for dependency in $(find . -maxdepth 1 -mindepth 1 -type d);
	do
		echo "Begin $dependency build..."
		rm -rf ../$dependency-build-complete.txt ../$dependency-build-failed.txt

		echo "Running bp-script.sh..."
		cd $dependency
		/usr/bin/bp

		# Go to build directory again
		if [ $? -eq 0 ]; then
			touch ../$dependency-build-complete.txt
		else
			touch ../$dependency-build-failed.txt
		fi
		cd ..
		sudo apt-get -f -y install ./*.deb
		cd yuzu-build
	done

	echo ""
	echo "================================================================================="
	echo ""

	echo ""
	echo "=================== COPYING PACKAGES TO REPOSITORY ==============================="
	echo ""

	# TODO: Not sure why *.deb packages in debian/ subdirectory do not get copied
	cd $HOME
	mkdir -p $HOME/aat-signed-repository

	# Make sure *.deb packages in cache are not copied
	sudo apt-get clean
	sudo rm -rf /var/cache/apt/archives/*.deb

	mv *.deb $HOME/aat-signed-repository

	# Remove *-build-deps-* packages
	rm -rf aat-signed-repository/*-build-deps-*

	echo ""
	echo "================================================================================="
	echo ""

	# If no builds failed
	#cd $HOME/yuzu-build
	#if ! [ -f *"-build-failed.txt" ]; then
	#	echo "All packages built without errors, removing build directory"
	#	cd $HOME
	#	rm -rf yuzu-build
	#else
	#	echo "Errors ocurred when building packages!"
	#	cd $HOME
	#	exit 1
	#fi

EOF
	# Failed in some packages
	arch="amd64"

	mkdir -p $HOME/aat-signed-repository
	cd $HOME/debian-bullseye-$arch/home/builder/aat-signed-repository
	sudo find $HOME/debian-bullseye-$arch/home/builder/aat-signed-repository -name "*.deb" -exec mv {} $HOME/aat-signed-repository/ \;
	sudo rm -rf $HOME/debian-bullseye-$arch/home/builder/aat-signed-repository/*
	sudo chown -R debports:debports $HOME/aat-signed-repository
done

echo "FINISHED JOB ON $(date +'%Y-%m-%d %H:%M:%S')"

# Sign packages
bash /home/debports/sign-packages.sh

#!/bin/sh
HOME=/home/debports
sudo rm -rf $HOME/debian-bullseye-amd64-chromium/home/builder/build
sudo rm -rf $HOME/debian-bullseye-amd64-chromium/home/builder/*.deb
sudo rm -rf $HOME/debian-bullseye-amd64-chromium/home/builder/aat-security-signed-repository/*
sudo rm -rf $HOME/debian-bullseye-amd64-chromium/home/builder/aat-signed-repository/*

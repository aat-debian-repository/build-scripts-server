#!/bin/sh
# Remove package folder. Useful when something needs to be rebuilt.
# Sample: sudo find . -maxdepth 5 -name "python3.10" -type d -exec rm -rf {} \;

sudo find . -mindepth 5 -maxdepth 5 -name "$1" -type d -exec rm -rf {} \;
sudo find . -mindepth 5 -maxdepth 5 -name "$1-build-complete.txt" -type f -exec rm -rf {} \;


#!/bin/bash

#
# AAT-DEBIAN-REPO AUTOMATED BUILD SCRIPT FOR SERVERS (CHROMIUM ONLY)
#
# The MIT License (MIT)
# Copyright © 2022 Antoni Aloy Torrens <aaloytorrens@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”),
# to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
# https://mit-license.org/
#


# We will build packages inside a debian chroot called debian-bullseye-$arch-chromium
# NOTES to build the chroot:
# 	* Bootstrap the root filesystem -> `sudo debootstrap --arch=amd64 bullseye debian-bullseye-$arch-chromium`
#	* Enter the chroot -> `sudo apt install arch-install-scripts -y && sudo arch-chroot debian-bullseye-$arch-chromium /bin/bash`
#	* Inside the chroot:
#		* Install git for downloading needed GitLab repositories -> `sudo apt install git`
#		* Install dpkg-dev and devscripts for building Debian packages -> `sudo apt install devscripts dpkg-dev`
#		* Install dpkg-sig for signing debian and debmake packages -> `sudo apt install dpkg-sig`
#		* Install makedeb -> See https://www.makedeb.org -> Easier way to build package
#		* Install reprepro for setting up Debian repositories -> `sudo apt install reprepro`
#		* Allow sudo group to execute any command without password -> `sudo visudo` and search for it
#		* Create a standard user called builder, no password -> `sudo adduser builder && sudo adduser builder sudo`
#		* Allow sudo commands without password -> `sudo visudo`
#		* Login as builder -> `su - builder`
#		* Clone the repository -> `git clone https://gitlab.com/antoni.aloytorrens/debrepo packages`
#	* Ready to run this script! -> Packages are classified in two groups: the packages that are built the Debian way and the packages built with makedeb.

# Exit on CTRL+C
trap "exit" INT

# Exit on any error -> FIXME: Some commands, such as adding user, etc. might be taken as errors
#set -eu

# Packaging status (bullseye)
# Chromium -> Enables Hangouts / Google Chat screen sharing extension
# We have made a special script for Chromium, as this package has lots of updates and secfixes.
# Other packages are not that rellevant unless they are bugfix, but this is.

# TODO: Make prebuilt rootfs with all of the above.

# TODO: Check everything instead of removing it!
# We need a flag to remove everything

# Remove everything if we mess up (uncomment this if you want to start again)
#sudo rm -rf debian-bullseye-$arch-chromium/home/builder/*

# Check if chromium needs to be built or not, according to version.
# Return 0: true
# Return 1: false
is_needed_build(){
	if [ -f "debian-bullseye-$arch-chromium/home/builder/latest-build.txt" ]; then
		# Get upstream version and compare to our latest build
		LOCAL_VERSION=$(cat "debian-bullseye-$arch-chromium/home/builder/latest-build.txt")

		# Get latest version (the first one) from Chromium
		# The first declaration is commented, as I need to have Chromium package installed
		#CHROMIUM_UPSTREAM_VERSION=$(dpkg-query --show --showformat '${Version}' chromium)
		CHROMIUM_UPSTREAM_VERSION=$(apt-cache madison chromium | grep bullseye | cut -d \| -f 2 | head -n1 | xargs)

		echo "Comparing Local Version with the Upstream one..."
		echo "LOCAL_VERSION: $LOCAL_VERSION; CHROMIUM_UPSTREAM_VERSION: ${CHROMIUM_UPSTREAM_VERSION}"
		NEEDS_BUILD=$(dpkg --compare-versions $LOCAL_VERSION lt $CHROMIUM_UPSTREAM_VERSION && echo "true" || echo "false")
		if [ $NEEDS_BUILD == "true" ]; then
			echo "Chromium needs building. Building Chromium from source..."
			return 0
		fi
		echo "Chromium does not need building. Done!"
		echo "FINISHED JOB ON $(date +'%Y-%m-%d %H:%M:%S')"
		return 1
	else
		echo "latest-build.txt does not exist. Building Chromium from source..."
		return 0
	fi
}


ARCHES=( "amd64" )
REPO_DEPENDENCIES=false
for arch in ${ARCHES[@]}; do
	cd $HOME

	# Report time and date
	echo "STARTED JOB AT $(date +'%Y-%m-%d %H:%M:%S')"

	# Update and cleanup
	echo "Update and cleanup..."
	sudo apt-get update >/dev/null; sudo apt-get -yq full-upgrade >/dev/null; sudo apt-get -y autoremove >/dev/null; sudo apt-get clean

	# Check if is needed build
	if ! is_needed_build; then
		exit 0
	fi

	# Remove contents of root (if you do CTRL+C in signing packages, the script behaves abnormally and could write files inside root folder)
	echo "Removing contents of /root in rootfs..."
	sudo rm -rf debian-bullseye-$arch-chromium/root/*

	# Copy /etc/hosts file
	echo "Copying /etc/hosts file..."
	sudo cp /etc/hosts debian-bullseye-$arch-chromium/etc/hosts

	# Copy sudoers file
	echo "Copying sudoers file..."
	sudo mkdir -p debian-bullseye-$arch-chromium/etc/sudoers.d/
	sudo cp etc-conf/sudoers.d/builder debian-bullseye-$arch-chromium/etc/sudoers.d/builder

	# Copy sources.list
	echo "Copying chromium sources.list..."
	sudo cp etc-conf/apt/chromium-sources.list debian-bullseye-$arch-chromium/etc/apt/sources.list

	# Copy cronjob
	echo "Copying chromium cronjob..."
	sudo cp etc-conf/cron.daily/chromium-build-check /etc/cron.daily/chromium-build-check
	sudo chmod +x /etc/cron.daily/chromium-build-check

	# Copy keys
	echo "Copying keys..."
	sudo cp keys/debports.rsa.secret.gpg debian-bullseye-$arch-chromium/home/builder/
	sudo cp keys/debports.rsa.pub.gpg debian-bullseye-$arch-chromium/home/builder/
	sudo cp usr-conf/share/keyrings/makedeb-archive-keyring.gpg debian-bullseye-$arch-chromium/usr/share/keyrings/makedeb-archive-keyring.gpg

	# Check gnupg files
	# https://d.sb/2016/11/gpg-inappropriate-ioctl-for-device-errors
	echo "Checking gnupg..."
	if ! [ -f debian-bullseye-$arch-chromium/home/builder/.gnupg/gpg-agent.conf ] || ! [ -f debian-bullseye-$arch-chromium/home/builder/.gnupg/gpg.conf ]; then
		sudo touch debian-bullseye-$arch-chromium/home/builder/restart-gnupg-service.txt
		sudo mkdir -p debian-bullseye-$arch-chromium/home/builder/.gnupg/
		sudo cp gnupg/gpg-agent.conf debian-bullseye-$arch-chromium/home/builder/.gnupg/gpg-agent.conf
		sudo cp gnupg/gpg.conf debian-bullseye-$arch-chromium/home/builder/.gnupg/gpg.conf
	else
		sudo rm -rf debian-bullseye-$arch-chromium/home/builder/restart-gnupg-service.txt
	fi

	# Check repository files
	#if ! [ -d debian-bullseye-$arch-chromium/home/builder/repo-conf ]; then
	echo "Checking repository files..."
	sudo cp -r repo-conf debian-bullseye-$arch-chromium/home/builder/
	#fi

	# Enter the chroot and execute commands
	echo "Done. Entering chroot..."
	sudo arch-chroot debian-bullseye-$arch-chromium /bin/bash -x <<'EOF'

	# Try to fix if some attempts were unsuccessful
	echo "Checking for previous failures..."
	apt -f install -y >/dev/null
	dpkg --configure -a >/dev/null
	echo "Done."

	# Add builder user into sudo group
	echo "Adding builder user into sudo group..."
	adduser builder sudo >/dev/null 2>&1

	echo "Updating rootfs, and cleaning up..."
	# Update and cleanup
	sudo apt update >/dev/null; sudo apt-get -yq full-upgrade >/dev/null; sudo apt-get -y autoremove >/dev/null; sudo apt-get clean

	# NOTE: We will use prebuilt rootfs instead
	#if [ -f /debootstrap/debootstrap ]; then
	#	debootstrap/debootstrap --second-stage
	#fi

	# Install required packages
	echo "Installing required packages..."
	apt-get -y install locales git dpkg-dev equivs devscripts dpkg-sig reprepro ash >/dev/null

	# Install sudo and ca-certificates for key importing
	echo "Installing sudo and ca-certificates for key importing..."
	apt-get -y install sudo ca-certificates >/dev/null

	# Configure locales
	echo "Configuring locales..."
	apt-get -y install debconf-utils >/dev/null 2>&1
	echo "locales locales/default_environment_locale select es_ES.UTF-8" | debconf-set-selections
	echo "locales locales/locales_to_be_generated multiselect es_ES.UTF-8 UTF-8" | debconf-set-selections
	rm "/etc/locale.gen"
	dpkg-reconfigure --frontend noninteractive locales >/dev/null 2>&1

	# Log in as builder
	echo "Logging in as builder..."
	su - builder

	# Change ownership and import keys
	echo "Changing ownership, importing keys..."
	sudo chown -R builder:builder $HOME/debports.rsa.secret.gpg
	sudo chown -R builder:builder $HOME/debports.rsa.pub.gpg

	# Fix gnupg permissions
	echo "Fixing gnupg permissions..."
	sudo chown -R builder:builder $HOME/.gnupg/
	sudo chmod 600 $HOME/.gnupg/*
	sudo chmod 700 $HOME/.gnupg/

	echo RELOADAGENT | gpg-connect-agent

	# --batch: https://superuser.com/questions/1325862/gnupg2-suddenly-throwing-error-building-skey-array-no-such-file-or-directory
	gpg --import --batch debports.rsa.pub.gpg
	gpg --allow-secret-key-import --batch --import debports.rsa.secret.gpg

	# Change owner of repository configurations
	sudo chown -R builder:builder $HOME/repo-conf

	# Create build directory
	echo "Creating build directory..."
	mkdir -p build
	cd build

	# Retrieve chromium source
	echo "Retrieving Chromium source code..."
	apt-get source chromium >/dev/null
	CHROMIUM_UPSTREAM_SRC=$(apt-cache madison chromium | cut -d \| -f 2 | cut -d \- -f 1 | head -n1 | xargs)
	cd "chromium-$CHROMIUM_UPSTREAM_SRC"

	echo ""
	echo "======================== BEGIN chromium build ========================================"
	echo ""

	rm -rf ../chromium-build-complete.txt ../chromium-build-failed.txt

	echo "Installing build dependencies..."
	mk-build-deps >/dev/null
	sudo apt -f install ./*.deb -y >/dev/null 2>&1
	sudo apt-get clean

	# Enable Hangouts / Google Chat screen sharing extension
	echo "Enabling Hangouts / Google Chat screen sharing extension..."
	sed -i 's/enable_hangout_services_extension=false/enable_hangout_services_extension=true/g' debian/rules

	# BUILD SILENTLY and write failing packages into log. TODO: Report failing packages
	echo "Building Chromium from source..."
	dpkg-buildpackage -b -us -ui -uc

	# Go to build directory again
	cd ..

	# Build succeeded: deb exists. Build not succeeded: deb does not exist.
	if [ -f chromium_${CHROMIUM_UPSTREAM_SRC}*.deb ]; then
		touch chromium-build-complete.txt
	else
		echo "$(pwd)/chromium_${CHROMIUM_UPSTREAM_SRC}*.deb"
		touch chromium-build-failed.txt
	fi

	# If build is succeeded, make latest-build.txt point to this new version
	if [ -f "chromium-build-complete.txt" ]; then
		CHROMIUM_LOCAL_VERSION=$(dpkg --info chromium_${CHROMIUM_UPSTREAM_SRC}*.deb | grep Version | cut -c 10-)
		echo $CHROMIUM_LOCAL_VERSION > ../latest-build.txt
	fi

	echo ""
	echo "======================== END chromium build ========================================"
	echo ""

	echo "Changing directory back to $HOME"
	cd $HOME

	echo ""
	echo "======================== CLEANING UP ========================================"
	echo ""

	# TODO (UPDATE): Test now with a clean build, I have modified it according to https://serverfault.com/a/1095084
	yes | sudo DEBIAN_FRONTEND="noninteractive" apt-get -yqq purge *-build-deps* >/dev/null
	yes | sudo DEBIAN_FRONTEND="noninteractive" apt-get -yqq autoremove >/dev/null
	sudo apt-get clean
	mv build/*.deb . >/dev/null 2>&1

	echo ""
	echo "================================================================================="
	echo ""

	echo ""
	echo "=================== COPYING PACKAGES TO REPOSITORY ==============================="
	echo ""

	# TODO: Not sure why *.deb packages in debian/ subdirectory do not get copied
	cd $HOME
	mkdir -p $HOME/aat-security-signed-repository

	# Make sure *.deb packages in cache are not copied
	sudo apt-get clean
	sudo rm -rf /var/cache/apt/archives/*.deb

	cp -f *.deb $HOME/aat-security-signed-repository

	# Remove *-build-deps-* packages
	rm -rf aat-security-signed-repository/*-build-deps-*

	echo ""
	echo "================================================================================="
	echo ""

EOF
	# Failed in some packages
	arch="amd64"

	mkdir -p $HOME/aat-security-signed-repository
	cd $HOME/debian-bullseye-$arch-chromium/home/builder/aat-security-signed-repository
	sudo find $HOME/debian-bullseye-$arch-chromium/home/builder/aat-security-signed-repository -name "*.deb" -exec cp -f {} $HOME/aat-security-signed-repository/ \;
	sudo rm -rf $HOME/debian-bullseye-$arch-chromium/home/builder/aat-security-signed-repository/*
	sudo chown -R debports:debports $HOME/aat-security-signed-repository
done

echo "FINISHED JOB ON $(date +'%Y-%m-%d %H:%M:%S')"

# Sign packages
bash /home/debports/sign-packages-chromium.sh

# Clean environment
bash /home/debports/clean-chromium-builder.sh

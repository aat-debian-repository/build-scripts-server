#!/bin/sh
# WORKAROUND FOR UNSUPPORTED ZSTD COMPRESSION IN BULLSEYE
cd $HOME
sudo rm -rf /var/www/aat-jammy-repository-bak
sudo mv /var/www/aat-jammy-repository /var/www/aat-jammy-repository-bak
sudo mkdir -p /var/www/aat-jammy-repository
sudo cp -r ubuntu-jammy-amd64/home/builder/repo-jammy-signed-conf /var/www/aat-jammy-repository/ubuntu

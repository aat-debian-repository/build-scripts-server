#!/bin/bash

#
# AAT-DEBIAN-REPO AUTOMATED BUILD SCRIPT FOR SERVERS
#
# The MIT License (MIT)
# Copyright © 2022 Antoni Aloy Torrens <aaloytorrens@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”),
# to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
# https://mit-license.org/
#


# We will build packages inside a debian chroot called debian-bullseye-$arch
# NOTES to build the chroot:
# 	* Bootstrap the root filesystem -> `sudo debootstrap --arch=amd64 bullseye debian-bullseye-$arch`
#	* Enter the chroot -> `sudo apt install arch-install-scripts -y && sudo arch-chroot debian-bullseye-$arch /bin/bash`
#	* Inside the chroot:
#		* Install git for downloading needed GitLab repositories -> `sudo apt install git`
#		* Install dpkg-dev and devscripts for building Debian packages -> `sudo apt install devscripts dpkg-dev`
#		* Install dpkg-sig for signing debian and debmake packages -> `sudo apt install dpkg-sig`
#		* Install makedeb -> See https://www.makedeb.org -> Easier way to build package
#		* Install reprepro for setting up Debian repositories -> `sudo apt install reprepro`
#		* Allow sudo group to execute any command without password -> `sudo visudo` and search for it
#		* Create a standard user called builder, no password -> `sudo adduser builder && sudo adduser builder sudo`
#		* Allow sudo commands without password -> `sudo visudo`
#		* Login as builder -> `su - builder`
#		* Clone the repository -> `git clone https://gitlab.com/antoni.aloytorrens/debrepo packages`
#	* Ready to run this script! -> Packages are classified in two groups: the packages that are built the Debian way and the packages built with makedeb.

# Packaging status (bullseye)
# blender -> 2.83 LTS bugfixes (until 2.83.20) and now follows 2.93 LTS release
# kdenlive -> bullseye backports, only bugfix release, no newer versions
# inkscape -> bullseye backports, only bugfix release, no newer versions
# gimp -> follows stable 2.10.x branch for bugfixes. babl and gegl libraries DO only depend on GIMP
# filezilla and libfilezilla -> follows latest release. Filezilla depends on libfilezilla.
# libsdl2 -> follows latest release
# lbreakouthd -> backport (follows latest release)
# lpairs2 -> backport (follows latest release)
# mysql-workbench and mysql-connector-python -> backport (follows latest 8.x release), zero backported libs if possible
# Linux Mint software (hypnotix, mintstick, mintinstall, webapp-manager, mintupdate, debian-system-adjustments) -> backport (follows latest release). hypnotix depends on xapp

# Packaging status (debmake)
# All debmake packages follows latest release, except Python3.10

# TODO: Make prebuilt rootfs with all of the above.

# TODO: Check everything instead of removing it!
# We need a flag to remove everything
# We need a flag to build single package (override PACKAGES_DEBIAN and PACKAGES_DEBMAKE)


# See https://stackoverflow.com/a/33826763
rmall=false
no_download=true
while [[ "$#" -gt 0 ]]; do
    case $1 in
        -r|--rmall) rmall=true; shift ;;
        -n|--download) no_download=false ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done

# TODO: Since in chroot I'm providing $arch as the second argument,
# make the chroot process a separate script called chroot-build.sh
# and override this function
print_info () {
	if [ $2 ];
	then
		echo "CHROOT [$2]: $1"
	else
		echo "INFO: $1"
	fi
}

# Remove everything if we mess up (uncomment this if you want to start again)
#sudo rm -rf debian-bullseye-$arch/home/builder/*
#ARCHES=( "amd64" "i386" "arm64" "armhf" "armel" )
ARCHES=( "amd64" )
REPO_DEPENDENCIES=false
for arch in ${ARCHES[@]}; do
	cd $HOME

	# Update host sources.list
	print_info "Updating sources.list..."
	sudo cp etc-conf/apt/host.list /etc/apt/sources.list

	# Allow insecure repositories if linuxmint-keyring is not installed
	print_info "Installing linuxmint-keyring..."
	sudo apt-get update --allow-insecure-repositories >/dev/null; sudo apt-get -y install linuxmint-keyring --allow-unauthenticated >/dev/null;

	# Update and cleanup
	print_info "Update and cleanup..."
	sudo apt-get -yq full-upgrade >/dev/null; sudo apt-get -y autoremove >/dev/null; sudo apt-get clean

	# Download prebuilt packages if option is not set (default)
	if [ "$no_download" = false ];
	then
		# Download prebuilt packages
		# These are packages that are in backports and we want to include them in our repo
		rm -rf $HOME/prebuilt-packages
		mkdir -p $HOME/prebuilt-packages

		# kdenlive to libmlt++-dev: used for updating kdenlive
		# inkscape, inkscape-tutorials: used for updating inkscape
		# libdecor: used for updating libsdl2
		PREBUILT_PACKAGES=( "kdenlive" "kdenlive-data" "python3-mlt" "melt" "libmlt6" "libmlt-dev" "libmlt-data" "libmlt++3" "libmlt++-dev" "inkscape" "inkscape-tutorials" "libdecor-0-0" )
		cd $HOME/prebuilt-packages
		# If package downloading fails, means that package is for all architectures
		for prebuilt_package in ${PREBUILT_PACKAGES[@]}; do
			# TODO: Version checking
			#if [ -f "$prebuilt_package*.deb" ]; then
			#	echo "Already downloaded! Skipping."
			#else
			#	apt download -t bullseye-backports $prebuilt_package:$arch || apt download -t bullseye-backports $prebuilt_package:all
			#fi
			apt download -t bullseye-backports $prebuilt_package:$arch || apt download -t bullseye-backports $prebuilt_package:all
		done
		cd $HOME

		cd $HOME/prebuilt-packages
		# Other previous packages are needed for mintinstall, mintupdate
		PREBUILT_LM_PACKAGES=( "aptdaemon" "aptdaemon-data" "mint-common" "mint-translations" "python3-aptdaemon" "python3-aptdaemon.gtk3widgets" "app-install-data" )
		# Same but for linuxmint repos
		for prebuilt_package in ${PREBUILT_LM_PACKAGES[@]}; do
			apt download -t elsie $prebuilt_package:$arch || apt download -t elsie $prebuilt_package:all
		done
	fi

	cd $HOME
	# Remove contents of root (if you do CTRL+C in signing packages, the script behaves abnormally and could write files inside root folder)
	print_info "Removing residual /root/* files..."
	sudo rm -rf debian-bullseye-$arch/root/*

	# Copy /etc/hosts file
	echo "Copying /etc/hosts file..."
	sudo cp /etc/hosts debian-bullseye-$arch/etc/hosts

	# Copy sudoers file
	print_info "Copying sudoers file..."
	sudo mkdir -p debian-bullseye-$arch/etc/sudoers.d/
	sudo cp etc-conf/sudoers.d/builder debian-bullseye-$arch/etc/sudoers.d/builder

	# Copy sources.list and custom repo
	print_info "Copying sources.list and custom repo..."
	sudo cp etc-conf/apt/sources.list debian-bullseye-$arch/etc/apt/sources.list
	sudo cp etc-conf/apt/sources.list.d/aat.list debian-bullseye-$arch/etc/apt/sources.list.d/aat.list
	sudo cp etc-conf/apt/sources.list.d/elsie.list debian-bullseye-$arch/etc/apt/sources.list.d/elsie.list
	sudo cp etc-conf/apt/sources.list.d/makedeb.list debian-bullseye-$arch/etc/apt/sources.list.d/makedeb.list

	# Always prefer packages from main repository rather than the custom ones
	print_info "Copying apt preferences..."
	sudo cp etc-conf/apt/preferences.d/89_aat-repository debian-bullseye-$arch/etc/apt/preferences.d/89_aat-repository

	# Copy keys
	print_info "Copying keys..."
	sudo cp keys/debports.rsa.secret.gpg debian-bullseye-$arch/home/builder/
	sudo cp keys/debports.rsa.pub.gpg debian-bullseye-$arch/home/builder/
	sudo cp usr-conf/share/keyrings/makedeb-archive-keyring.gpg debian-bullseye-$arch/usr/share/keyrings/makedeb-archive-keyring.gpg

	# Check gnupg files
	print_info "Checking gnupg..."
	# https://d.sb/2016/11/gpg-inappropriate-ioctl-for-device-errors
	if ! [ -f debian-bullseye-$arch/home/builder/.gnupg/gpg-agent.conf ] || ! [ -f debian-bullseye-$arch/home/builder/.gnupg/gpg.conf ]; then
		sudo touch debian-bullseye-$arch/home/builder/restart-gnupg-service.txt
		sudo mkdir -p debian-bullseye-$arch/home/builder/.gnupg/
		sudo cp gnupg/gpg-agent.conf debian-bullseye-$arch/home/builder/.gnupg/gpg-agent.conf
		sudo cp gnupg/gpg.conf debian-bullseye-$arch/home/builder/.gnupg/gpg.conf
	else
		sudo rm -rf debian-bullseye-$arch/home/builder/restart-gnupg-service.txt
	fi

	# Check repository files
	#if ! [ -d debian-bullseye-$arch/home/builder/repo-conf ]; then
	sudo cp -r repo-conf debian-bullseye-$arch/home/builder/
	#fi

	# Enter the chroot and execute commands
	sudo arch-chroot debian-bullseye-$arch /bin/bash -x <<'EOF'

	# Stop at any error
	set -eu

	# TODO: Since in chroot I'm providing $arch as the second argument,
	# make the chroot process a separate script called chroot-build.sh
	# and override this function
	print_info () {
		echo "INFO: $1"
	}


	# Try to fix if some attempts were unsuccessful
	print_info "Try to fix if some attempts were unsuccessful..."
	apt-get -f -y install >/dev/null
	dpkg --configure -a >/dev/null

	print_info "Installing sudo..."
	sudo apt-get -y install sudo >/dev/null

	# Add builder user into sudo group
	print_info "Adding builder user into sudo group..."
	adduser builder sudo >/dev/null

	# Install linuxmint-keyring
	print_info "Installing linuxmint-keyring..."
	sudo apt-get update --allow-insecure-repositories >/dev/null; sudo apt-get -y install linuxmint-keyring --allow-unauthenticated >/dev/null;

	# Update and cleanup
	print_info "Update and cleanup..."
	sudo apt-get update >/dev/null; sudo apt-get -yq --allow-downgrades full-upgrade >/dev/null; sudo apt-get -y autoremove >/dev/null; sudo apt-get clean

	# NOTE: We will use prebuilt rootfs instead
	#if [ -f /debootstrap/debootstrap ]; then
	#	debootstrap/debootstrap --second-stage
	#fi

	# Install required packages
	print_info "Installing required packages..."
	sudo apt-get -y install locales git dpkg-dev equivs devscripts dpkg-sig reprepro ash >/dev/null
	sudo apt-get -y install makedeb >/dev/null

	# Install ca-certificates for key importing
	print_info "Installing ca-certificates for key importing..."
	apt-get -y install sudo ca-certificates >/dev/null

	# Configure locales
	print_info "Configuring locales..."
	apt install -y debconf-utils >/dev/null
	echo "locales locales/default_environment_locale select es_ES.UTF-8" | debconf-set-selections
	echo "locales locales/locales_to_be_generated multiselect es_ES.UTF-8 UTF-8" | debconf-set-selections
	rm "/etc/locale.gen"
	dpkg-reconfigure --frontend noninteractive locales >/dev/null

	# Install mariadb-server because there is a prompt asking for uninstallation, and I do not know how to make it non_interactive
	# Depends on 'mysql-workbench' and 'meowsql' packages.
	# As I am using sudo for apt, perhaps setting a global variable might work: https://www.cyberciti.biz/faq/explain-debian_frontend-apt-get-variable-for-ubuntu-debian/
	# But this does not work: https://serverfault.com/questions/790118/attempting-to-purge-mysql-with-apt-get-unable-to-do-so-without-interaction
	print_info "WORKAROUND: Installing mariadb-server..."
	apt-get -y install mariadb-server >/dev/null

	# Install phonon4qt5-backend-vlc because there is a prompt asking for non-functional package, and I do not know how to make it non_interactive
	# Depends on 'purpose' packages.
	# This is the message dialog:
	# Falta el motor de Phonon4Qt5                                                                                                                                          │
 	#│                                                                                                                                                                       │
 	#│ Las aplicaciones que utilizan Phonon4Qt5 (la infraestructura multimedia KF 5) no producirán ninguna salida de vídeo o audio, porque solo se ha instalado un motor     │
 	#│ simulado en el sistema. Normalmente, esta es una configuración indeseada.                                                                                             │
 	#│                                                                                                                                                                       │
 	#│ Para restablecer completamente las capacidades multimedia de Phonon4Qt5, instale alguno de los paquetes de motores reales de Phonon4Qt5 disponibles en este sistema:  │
 	#│                                                                                                                                                                       │
 	#│ phonon4qt5-backend-vlc (recommended), phonon4qt5-backend-gstreamer
	print_info "WORKAROUND: Installing phonon4qt5 and vlc backend..."
	apt-get -y install phonon4qt5 phonon4qt5-backend-vlc >/dev/null

	# FIXME: Previous config files copy contents into /home/builder already
	# Adduser builder
	# useradd -s /bin/bash -m -G sudo builder

	# Log in as builder
	su - builder

	# Change ownership and import keys
	print_info "Changing gnupg key ownership..."
	sudo chown -R builder:builder $HOME/debian
	sudo chown -R builder:builder $HOME/debmake
	sudo chown -R builder:builder $HOME/debports.rsa.secret.gpg
	sudo chown -R builder:builder $HOME/debports.rsa.pub.gpg

	# Fix gnupg permissions
	print_info "Checking gnupg..."
	sudo chown -R builder:builder $HOME/.gnupg/
	sudo chmod 600 $HOME/.gnupg/*
	sudo chmod 700 $HOME/.gnupg/

	# Check if gnupg service has to be restarted
	#if [ -f $HOME/restart-gnupg-service.txt ]; then
	#	echo RELOADAGENT | gpg-connect-agent
	#fi

	echo RELOADAGENT | gpg-connect-agent

	# --batch: https://superuser.com/questions/1325862/gnupg2-suddenly-throwing-error-building-skey-array-no-such-file-or-directory
	print_info "Importing gnupg keys..."
	gpg --import --batch debports.rsa.pub.gpg
	gpg --allow-secret-key-import --batch --import debports.rsa.secret.gpg

	# Change owner of repository configurations
	sudo chown -R builder:builder $HOME/repo-conf

	# Variables

	REPOS_URL="https://gitlab.com/aat-debian-repository"
	HOME="/home/builder"
	REPOS_DEBIAN_URL="$REPOS_URL/debian"
	LOCAL_DEBIAN_URL="$HOME/debian"
	REPOS_DEBMAKE_URL="$REPOS_URL/makedeb"
	LOCAL_DEBMAKE_URL="$HOME/debmake"

	# purpose is needed to be rebuilt for kdenlive, as we do not want kdeconnect integration into GNOME
	# gimp -> depends on newer version of gegl ; gegl -> depends on newer version of babl
	# xapp is needed for hypnotix
	# meta-gnome3 -> depends on mintinstall, webapp-manager, mintstick and debian-system-adjustments
	# virtualbox -> depends on virtualbox-guest-additions-iso
	# io.elementaryos.code -> depends on granite6
	# prismlauncher -> depends on tomlplusplus, libghc-filesystem and quazip1-qt5
	#PACKAGES_DEBIAN=( "mintinstall" "webapp-manager" "debian-system-adjustments" "chromium-tweaks" "mintupdate" "meta-gnome3" "xapp" "hypnotix" "mintstick" "purpose" "babl" "gegl" "gimp" "libsdl2" "libsdl2-gfx" "libsdl2-image" "libsdl2-mixer" "libsdl2-net" "libsdl2-ttf" "sdl12-compat" "lbreakouthd" "lpairs2" "blender" "pitivi" "mysql-workbench" "mysql-connector-python" "virtualbox-guest-additions-iso" "virtualbox" "notepadqq" "granite6" "io.elementaryos.code" "musescore3" "musescore4" "pipe-viewer" "grandorgue" "cpupower-gui" "libfilezilla" "filezilla" "kdbg" "evolution-indicator" "tomlplusplus" "libghc-filesystem" "libquazip1-qt5" "prismlauncher" "bottles" "reaper-downloader" )
	#PACKAGES_DEBMAKE=( "pycharm-community" "python3.10" "python3.11" "meowsql" "fractal" "clockify-desktop" "clickup" "postman" "onivim2" "libversion" "itch" "gamejolt" "lagrange" "castor-gtk" )

	PACKAGES_DEBIAN=( "reaper-downloader" )
	PACKAGES_DEBMAKE=( "" )

	# Clone repositories
	# Packages built "the Debian way"
	echo "Cloning own Debian repositories..."
	for package in ${PACKAGES_DEBIAN[@]}; do
		# Create debian directory
		mkdir -p debian
		cd debian

		repository="$REPOS_DEBIAN_URL/$package"
		local_repository="$LOCAL_DEBIAN_URL/$package"
		if [ -d "$package" ]; then
			echo "$repository repository already cloned, performing git pull..."
			cd "$package"
			git pull
			cd ..
		else
			echo "Cloning $repository..."
			git clone --depth=1 "$repository".git "$package"
		fi

		builddir=$package
		cd $builddir

		if [ -f "../$package-build-complete.txt" ]; then
			print_info "SKIPPING $builddir build. Build already complete!"
		else
			print_info "BEGIN $builddir build..."

			# Remove residual file
			rm -rf ../$package-build-failed.txt

			# LOG EVERYTHING INTO CONSOLE AND FILE
			# dpkg-buildpackage -b -us -ui -uc 2>&1 | tee ../build-log-$builddir.txt && touch ../$package-build-complete.txt


			# FIXME: Make this dependency circle a lot cleaner (or remove gimp from builder)
			# For example, PostmarketOS makes an inhouse repo in /mnt/packages in each chroot,
			# adds it in sources.list and updates the packages

			# gegl depends on babl
			if [ "$package" == "gegl" ]; then
				sudo apt-get -f -y install ../*babl*.deb
			fi

			# FIXME: operador unario
			# gimp depends on gegl and babl
			if [ "$package" == "gimp" ]; then
				sudo apt-get -f -y install ../*babl*.deb ../*gegl*.deb
			fi

			# Filezilla depends on libfilezilla
			if [ "$package" == "filezilla" ]; then
				sudo apt-get -f -y install ../*libfilezilla*.deb
			fi

			# cpupower-gui depends on meson from backports
			if [ "$package" == "cpupower-gui" ]; then
				sudo apt-get -yq -t bullseye-backports install meson
			fi

			# tomlplusplus depends on meson from backports
			if [ "$package" == "tomlplusplus" ]; then
				sudo apt-get -yq -t bullseye-backports install meson
			fi

			# prismlauncher depends on tomlplusplus, libghc-filesystem and libquazip1-qt5
			if [ "$package" == "prismlauncher" ]; then
				sudo apt-get -f -y install ../*libghc-filesystem-dev*.deb ../libquazip1-qt5*.deb ../libtomlplusplus*.deb
			fi

			mk-build-deps
			sudo apt -f install ./*.deb -y
			sudo apt clean

			# BUILD SILENTLY and write failing packages into log. TODO: Report failing packages
			dpkg-buildpackage -b -us -ui -uc 2>&1 && touch ../$package-build-complete.txt || touch ../$package-build-failed.txt


			# TODO: local repo
			#if [ -f ../$package-build-complete.txt ];
			#then
			#	./sign-packages.sh
			#fi
			print_info "End $builddir build..."
		fi

		echo "Changing directory back to $HOME"
		cd $HOME
	done

	for package in ${PACKAGES_DEBMAKE[@]}; do
		# Create debian directory
		mkdir -p debmake
		cd debmake

		repository="$REPOS_DEBMAKE_URL/$package"
		local_repository="$LOCAL_DEBMAKE_URL/$package"

		if [ -d "$package" ]; then
			echo "$repository repository already cloned, skipping!"
		else

			echo "Cloning $repository..."
			git clone "$repository" "$package"
		fi

		cd $local_repository

		if [ -f "../$package-build-complete.txt" ]; then
			echo ""
			echo "=============== SKIPPING $(basename $local_repository) build. Build already complete. ==================="
			echo ""
		else
			echo ""
			echo "======================= BEGIN $(basename $local_repository) build ======================================="
			echo ""

			# Remove residual file
			rm -rf ../$package-build-failed.txt

			sudo apt install makedeb -y
			makedeb -s --no-confirm --skip-pgp-check && touch ../$package-build-complete.txt && REPO_DEPENDENCIES=false || touch ../$package-build-failed.txt

			echo ""
			echo "======================== END $(basename $local_repository) build ========================================"
			echo ""
		fi

		echo "Changing directory back to $HOME"
		# FIXME: noninteractive frontend *should* work, but it does not. That is why we install mariadb and libphone4-qt-vlc
		# TODO (UPDATE): Test now with a clean build, I have modified it according to https://serverfault.com/a/1095084
		yes | sudo DEBIAN_FRONTEND="noninteractive" apt-get -yqq purge *-build-deps*
		yes | sudo DEBIAN_FRONTEND="noninteractive" apt-get -yqq autoremove
		sudo apt clean
		cd $HOME
	done

	echo ""
	echo "======================== ALL BUILDS DONE ========================================"
	echo ""

	echo "TODO: Make report, show successful build packages and the failing ones. Output them to console and to a file."

	echo ""
	echo "================================================================================="
	echo ""

	echo ""
	echo "======================== CLEANING UP ========================================"
	echo ""

	# Cleaning src and tmp files from debian directories, and src from debmake directories to free up a lot of space.
	echo "Cleaning up src and tmp folders... Found the following:"
	find $HOME -mindepth 4 -maxdepth 4 -name "src"
	find $HOME -mindepth 4 -maxdepth 4 -name "tmp"
	find $HOME -mindepth 4 -maxdepth 4 -name "src" -exec rm -rf {} \;
	find $HOME -mindepth 4 -maxdepth 4 -name "tmp" -exec rm -rf {} \;


	echo ""
	echo "================================================================================="
	echo ""

	echo ""
	echo "=================== COPYING PACKAGES TO REPOSITORY ==============================="
	echo ""

	# TODO: Not sure why *.deb packages in debian/ subdirectory do not get copied
	cd $HOME
	mkdir -p $HOME/aat-signed-repository

	# Make sure *.deb packages in cache are not copied
	sudo apt clean
	sudo rm -rf /var/cache/apt/archives/*.deb

	# Redirect errors to /dev/null, lots of permission issues
	echo "Found the following packages: "
	find $LOCAL_DEBIAN_URL -name "*.deb" 2>/dev/null
	find $LOCAL_DEBMAKE_URL -name "*.deb" 2>/dev/null

	find $LOCAL_DEBIAN_URL -name "*.deb" -exec mv {} $HOME/aat-signed-repository \;
	find $LOCAL_DEBMAKE_URL -name "*.deb" -exec mv {} $HOME/aat-signed-repository \;

	# Remove *-build-deps-* packages
	for package in $HOME/aat-signed-repository/*.deb; do
		if [[ $(basename "$package") == *"-build-deps"* ]]; then
			rm -rf $package
		fi
	done

	echo ""
	echo "================================================================================="
	echo ""

EOF
	mkdir -p $HOME/aat-signed-repository
	cd $HOME/debian-bullseye-$arch/home/builder/aat-signed-repository
	sudo find . -name "*.deb" -exec mv {} $HOME/aat-signed-repository/ \;
	sudo rm -rf $HOME/debian-bullseye-$arch/home/builder/aat-signed-repository/*
	sudo chown -R debports:debports $HOME/aat-signed-repository
done

source $HOME/sign-packages.sh

# FIXME: Debian Bullseye's dpkg does not support zstd compression.
# Workaround: Sign packages using ubuntu-jammy rootfs and copy generated repo outside chroot.
echo ""
echo "======================== SIGNING PACKAGES ========================================"
echo ""

# FIXME: Make this an external script. As gpg asks for password, we cannot run this in background.

cd $HOME/aat-jammy-signed-repository

# FIXME: This loop has already been implemented before
# Remove *-build-deps-* packages
for package in "$HOME/aat-jammy-signed-repository/*.deb"; do
        if [[ $(basename "$package") == *"-build-deps"* ]]; then
                rm -rf $package
        fi
done
# TODO: Check if the packages are already inside repo-signed-conf
# If they are equal, do not sign them, as they are the same (same package name, same version)
# Otherwise we'll have https://askubuntu.com/questions/41605/trouble-downloading-packages-list-due-to-a-hash-sum-mismatch-error in users
find . -name "*.deb" -exec dpkg-sig --sign builder {} \;

echo ""
echo "================================================================================="

echo ""
echo "======================== SETTING UP REPREPRO ========================================"
echo ""

rm -rf $HOME/repo-jammy-signed-conf
mkdir -p $HOME/repo-jammy-signed-conf
cp -r $HOME/repo-jammy-conf/* $HOME/repo-jammy-signed-conf

# Remove existing folders so reprepro can set them up again
# Reprepro cannot guess on which category belong the packages built with makedeb. We will set hardcoded options.
cd $HOME/repo-jammy-signed-conf
for debpackage in $HOME/aat-jammy-signed-repository/*.deb; do
        reprepro --ask-passphrase -Vb . includedeb jammy "$debpackage" || echo "Retrying with hardcoded options..."; \
        reprepro --ask-passphrase -Vb . -S utils -C main -P optional includedeb jammy "$debpackage"
done

echo ""
echo "================================================================================="
echo ""

echo "======================== SETTING UP FINAL DESTINATION ========================================"
sudo rm -rf /var/www/aat-jammy-repository/*
sudo mkdir -p /var/www/aat-jammy-repository/ubuntu
sudo cp -fr $HOME/repo-jammy-conf/* /var/www/aat-jammy-repository/debian

# All repos are signed with same key, pick gpg file from random arch
cd $HOME
sudo cp -v keys/debports.rsa.pub.gpg /var/www/aat-jammy-repository/aat-jammy-repository.rsa.pub.key
sudo chown -R root:root /var/www/aat-jammy-repository
